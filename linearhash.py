'''
  File: linearhash.py (As saved from linearhash.shell.)
  Author: Steve Hubbard and Isaac Davis
  Date: 15/10/13
  Description: This file defines a class, LinearHashTable, which 
    implements linear probing.  Instances of any class may be elements 
    of such a hash table if hash(anObject) and anObject == otherObject make
    sense.  If a == b, then it must be true that hash(a) == hash(b).
'''

from copy import deepcopy
from person import Person, Student

class LinearHashTable:
   def __init__(self, tableSize = 10, empty = -1, deleted = -2):
      self.tableSize = tableSize
      self.table = [empty]*tableSize
      self.empty = empty
      self.deleted = deleted

   def __str__(self):
      me = 'The size of the hash table is ' + \
             str(self.tableSize) + '\n'
      for index in range(len(self.table)):
          if self.table[index] != self.empty:
              me += str(index) + ': ' + \
                    str(self.table[index])+ '\n'
      return me

   def hash(self, anObject):
      return hash(anObject) % self.tableSize

   def _isPrime_(self, anInt):
      '''Answer True iff anInt is a prime integer.'''
      prime = True
      for x in range(2, anInt // 2 + 1):
         if (anInt % x == 0):
            prime = False
      return prime
    

   def search(self, item):
      '''
        This method returns a tuple (a,b).
        Case 1:  If we find a match, a is True and b is
          the index of the matching item.
        Case 2: If we do not find a match for item, a is False.
          If we encounter a deleted slot, b is the index of the
          first deleted slot. If we do not encounter a deleted slot
          but do encounter an empty slot, b is the index of the
          first emtpy slot. If we have a full table with no empty
          or deleted slots, b is negative.  In case 2, location (b) is
          the index of the slot where the object belongs, if there
          is room.
      '''
      addressToCheck  = self.hash(item)
      found = False
      foundADeleted = False
      count = 1
      firstDeletedSlot = None
      while not found and self.table[addressToCheck] != self.empty and count-1 != self.tableSize:
         if self.table[addressToCheck] == item:
            found = True
         elif self.table[addressToCheck] == self.deleted and not foundADeleted:
               firstDeletedSlot = addressToCheck
               foundADeleted = True
         else:
            addressToCheck = self.getNext(count, item)
            count += 1
      if not found:
         if self.table[addressToCheck] == self.empty and not foundADeleted:
            return found, addressToCheck 
         elif count-1 == self.tableSize and not foundADeleted:
            return found, -1
         else:
            return found, firstDeletedSlot
      return found, addressToCheck


   def insert(self, item):
      ''' Answer None if the method fails.  Otherwise we inserted
          a deep copy of item in the correct location and answer item.
      '''
      wasFound, atLocation = self.search(item)
      if wasFound or atLocation < 0:
         return None
      else:
         self.table[atLocation] = deepcopy(item)
         return item
        
   def delete(self, item):
      ''' Answer None if the method fails.  Otherwise we answer the
           corresponding entry in the hash table, and mark the slot as
           deleted.
      '''
      wasFound, atLocation = self.search(item)
      if wasFound:
         itemToReturn = self.table[atLocation]
         self.table[atLocation] = self.deleted
         return itemToReturn
      else:
         return None

   def retrieve(self, item):
      ''' Answer None if the method fails.  Otherwise answer a deep
            copy of the corresponding entry of the table.
      '''
      wasFound, atAddress = self.search(item)
      if wasFound:
         return deepcopy(self.table[atAddress])
      else:
         return None
            
            

   def update(self, newItem):
      ''' Answer None if the method fails.  Otherwise we inserted
          a deep copy of item in the correct location and answer newItem.
      '''
      wasFound, atAddress = self.search(newItem)
      if wasFound:
         self.table[atAddress] = deepcopy(newItem)
         return newItem
      else:
         return None

   def getNext(self, iteration, item):
      '''This is the method that implements the difference between the hash methods.
         The input paramater is the number of times that a collision has occured.
         It will return the index in the table that should be tried next.
      '''
      return (self.hash(item) + iteration) % self.tableSize

class LinearQHashTable(LinearHashTable):
   def __init__(self, tableSize = 10, empty = -1, deleted = -2):
      if(not self._isPrime_(tableSize)):
         print("Invalid table size of " +  str(tableSize) + " -- not a prime.")
      super().__init__(tableSize, empty, deleted)
      
   def getNext(self, iteration, item):
      amountToMove = (hash(item) // self.tableSize)%self.tableSize
      if amountToMove == 0:
         amountToMove = 1
      nextPlace = (iteration * amountToMove) + hash(item)
      return nextPlace %self.tableSize
      


class ExtendedQHashTable(LinearHashTable):
   def __init__(self, tableSize = 10, empty = -1, deleted = -2):
      if((tableSize-3)%4 != 0 or (not self._isPrime_(tableSize))):
         print("Need a prime table size of the form 4*k + 3, not " + str(tableSize) + ".")
      super().__init__(tableSize, empty, deleted)
      
   def getNext(self, iteration, item): #not right yet
      iteration -= 1
      homeAddress = self.hash(item)
      ammountToChange = (((iteration // 2)+1)**2) * (-1)**(iteration)
#       print  (str((homeAddress+ammountToChange) % self.tableSize) + " exht " + str(item.id))
      return (homeAddress+ammountToChange) % self.tableSize


def main():
    print("My name is Isaac Davis")
    print("Linear hashing:")
    h = LinearHashTable(5)
    h.table[0] = h.deleted
    h.table[2] = 7
    h.table[3] = h.deleted
    h.table[4] = 17
    print( h.search(17) ) # Should be (True, 4)
    print( h.search(22) ) # Should be (False, 3)
    print( h.search(11) ) # Should be (False, 1)
    print( h.search(19) ) # Should be (False, 0)
    h.table[0] =  19
    h.table[1] =  11
    h.table[3] =  22
    print( h.search(72) ) # Should be something like (False, -1)
    print( h )
   
    print("Linear quotient hashing:")
    h = LinearQHashTable(7)
    h.insert(10)
    h.insert(12)
    h.insert(17)
    h.insert(24)
    h.insert(18)
    h.insert(31)
    print(h)
    h.delete(24)
    print(h)
    h.insert(5)
    print(h)
    h = LinearQHashTable(70, -3)
    
    table = LinearQHashTable(5, empty = 'Nothing!', deleted = 'Gone!')
    table.insert(Person('Joe', 15))
    table.insert(Person('Jill', 3))
    table.insert(Person('Bill', 1))
    table.insert(Person('Maude', 19))
    table.delete(Person(" ", 3))
    print (table)
    table.insert(Person('Lennie', 23))
    print (table)
    print (table.retrieve(Person(" ",19)))
    print (table.retrieve(Person(" ",45)))
    print (table.update(Person('Billy Joe Bob', 1)))
    print (table.update(Person('Susie', 45)))
    print (table)
    
    table = ExtendedQHashTable(7)
    table.insert(Student('Joe', 15, 3.3))
    table.insert(Student('Jill', 3, 4.0))
    table.insert(Student('Bill', 1,2.99))
    table.insert(Student('Maude', 19, 0))
    print (table.delete(Student(" ",3, 0)))
    print (table.insert(Student('Lennie', 23, 3.87)))
    print (table)
    print (table.retrieve(Student(" ",19, 2.0)))
    print (table.retrieve(Student(" ",45, 5)))
    print (table.update(Student('Billy Joe Bob', 1, 3)))
    print (table.update(Student('Susie', 45, 3.13)))
    print (table.delete(Student(" ",33, 4)))
    print (table.delete(Student(" ", 1, 5)))
    print (table)

   
    print("Extended quadratic hashing:")
    h.insert(10)
    h.insert(11)
    h.insert(17)
    h.insert(24)
    h.insert(31)
    h.insert(3)
    print (h)    
    h.delete(24)
    print (h)
    h.insert(38)
    print (h)
    h = ExtendedQHashTable(8)
    h = ExtendedQHashTable(17)
    table = ExtendedQHashTable(7)
    table.insert(Person('Joe', 15))
    table.insert(Person('Jill', 3))
    table.insert(Person('Bill', 1))
    table.insert(Person('Maude', 19))
    print (table.delete(Person(" ",3)))
    print (table.insert(Person('Lennie', 23)))
    print (table)
    print (table.retrieve(Person(" ",19)))
    print (table.retrieve(Person(" ",45)))
    print (table.update(Person('Billy Joe Bob', 1)))
    print (table.update(Person('Susie', 45)))
    print (table.delete(Person(" ",33)))
    print (table.delete(Person(" ", 1)))
    print (table)
    
if __name__ == '__main__': main()
'''
[evaluate linearhash.py]
My name is 
Linear hashing:
(True, 4)
(False, 3)
(False, 1)
(False, 0)
(False, -1)
The size of the hash table is 5
0: 19
1: 11
2: 7
3: 22
4: 17

Linear quotient hashing:
The size of the hash table is 7
0: 17
1: 31
3: 10
4: 18
5: 12
6: 24

The size of the hash table is 7
0: 17
1: 31
3: 10
4: 18
5: 12
6: -2

The size of the hash table is 7
0: 17
1: 31
3: 10
4: 18
5: 12
6: 5

Invalid table size of 70 -- not a prime.
The size of the hash table is 5
0: Name: Joe Id: 15 
1: Name: Bill Id: 1 
3: Gone!
4: Name: Maude Id: 19 

The size of the hash table is 5
0: Name: Joe Id: 15 
1: Name: Bill Id: 1 
3: Name: Lennie Id: 23 
4: Name: Maude Id: 19 

Name: Maude Id: 19 
None
Name: Billy Joe Bob Id: 1 
None
The size of the hash table is 5
0: Name: Joe Id: 15 
1: Name: Billy Joe Bob Id: 1 
3: Name: Lennie Id: 23 
4: Name: Maude Id: 19 

None
Name: Lennie Id: 23 gpa: 3.87
The size of the hash table is 7
1: Name: Joe Id: 15 gpa: 3.3
2: Name: Bill Id: 1 gpa: 2.99
3: Name: Jill Id: 3 gpa: 4.0
5: Name: Maude Id: 19 gpa: 0
6: Name: Lennie Id: 23 gpa: 3.87

None
None
None
None
None
None
The size of the hash table is 7
1: Name: Joe Id: 15 gpa: 3.3
2: Name: Bill Id: 1 gpa: 2.99
3: Name: Jill Id: 3 gpa: 4.0
5: Name: Maude Id: 19 gpa: 0
6: Name: Lennie Id: 23 gpa: 3.87

Extended quadratic hashing:
The size of the hash table is 70
3: 3
10: 10
11: 11
17: 17
24: 24
31: 31

The size of the hash table is 70
3: 3
10: 10
11: 11
17: 17
24: -2
31: 31

The size of the hash table is 70
3: 3
10: 10
11: 11
17: 17
24: -2
31: 31
38: 38

Need a prime table size of the form 4*k + 3, not 8.
Need a prime table size of the form 4*k + 3, not 17.
Name: Jill Id: 3 
Name: Lennie Id: 23 
The size of the hash table is 7
1: Name: Joe Id: 15 
2: Name: Bill Id: 1 
3: Name: Lennie Id: 23 
5: Name: Maude Id: 19 

Name: Maude Id: 19 
None
Name: Billy Joe Bob Id: 1 
None
None
Name: Billy Joe Bob Id: 1 
The size of the hash table is 7
1: Name: Joe Id: 15 
2: -2
3: Name: Lennie Id: 23 
5: Name: Maude Id: 19 
'''
